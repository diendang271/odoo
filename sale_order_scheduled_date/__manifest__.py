# -*- coding: utf-8 -*-
{
    'name': "Sale Order Scheduled Date",

    'summary': """
        Add scheduled date, which time salesman starts to deliver the item to customer""",

    'description': """
        Add scheduled date, which time salesman starts to deliver the item to customer
    """,

    'author': "Vuabia",
    'website': "http://www.vuabia.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Sales',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}