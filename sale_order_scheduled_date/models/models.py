# -*- coding: utf-8 -*-

from datetime import timedelta
from odoo import api, fields, models, _


class SaleOrder(models.Model):
    """Add several date fields to Sale Orders, computed or user-entered"""
    _inherit = 'sale.order'

    scheduled_date = fields.Datetime('Scheduled Date', readonly=False, states={'draft': [('readonly', False)],
                                                                               'sent': [('readonly', False)]},
                                     copy=False,
                                     help="Date by which the salesman start deliver the items.")

